import unittest
from selenium import webdriver
from pages.BaseModule import BaseClass
from pages.HomeModule import HomePage
from pages.GlobalVars import UserCredentials


class SearchIssue(BaseClass):
    def test3_search_existing_issue(self):
        home = HomePage(self.driver)
        home.openHomepage()
        login = home.logInButton()
        login.enterCredentials(UserCredentials['name'], UserCredentials['password'])
        issue = home.searchLastIssue()
        issue.verySearchedIssue()

if __name__ == '__main__':
    unittest.main()
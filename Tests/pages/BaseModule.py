from selenium import webdriver
import unittest

class BaseClass(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()
        #cls.driver = webdriver.Chrome()
        cls.driver.maximize_window()
        cls.driver.implicitly_wait(5)
        

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
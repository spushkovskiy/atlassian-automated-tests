from pages.LogInModule import LoginPage
from pages.IssueModule import IssuePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time


IssueIdID = 'key-val'
IssueSummaryValID = 'summary-val'
EditIssueButtonId = 'edit-issue'
CreateIssueWindowXPATH = ".//*[@id='create-issue-dialog"

class IssueFound:

    def __init__(self,driver):
        self.driver = driver

    def verySearchedIssue(self):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.ID, IssueIdID)))
        with open('pages/temp.txt','r') as t:
            issueNumber = t.readline()

        assert issueNumber.strip() in self.driver.find_element_by_id(IssueIdID).text

    def editIssueButton(self):
        WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.ID, EditIssueButtonId)))
        self.driver.find_element_by_id(EditIssueButtonId).click()
        return IssuePage(self.driver)

    def verifyUpdate(self, new_summary):
        time.sleep(2)
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, IssueSummaryValID)))
        assert new_summary in self.driver.find_element_by_id(IssueSummaryValID).text


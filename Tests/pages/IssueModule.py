from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import time


IssueTypeId = 'issuetype-field'
SummaryId = 'summary'
DescriptionId = 'description'
CreateId = 'create-issue-submit'
UpdateButton = 'edit-issue-submit'
CreateIssueWindowID = 'create-issue-dialog'
IssueTypeFieldId = 'issuetype-field'
BugTypeId = ".//*[@id='bug-6']/a"
CreateIssueWindowXPATH = ".//*[@id='create-issue-dialog"


class IssuePage():

    def __init__(self, driver):
        self.driver = driver

    def createNewIssue(self, summary, description):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, SummaryId)))

        self.driver.find_element_by_id(SummaryId).send_keys(summary)
        self.driver.find_element_by_id(DescriptionId).send_keys(description)
        self.driver.find_element_by_id(CreateId).click()
        WebDriverWait(self.driver, 10).until_not(EC.visibility_of_element_located((By.ID, CreateIssueWindowID)))

    def changeSummary(self, new_summary):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, SummaryId)))
        self.driver.find_element_by_id(SummaryId).clear()
        self.driver.find_element_by_id(SummaryId).send_keys(new_summary)

    def changeDescription(self, new_description):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, DescriptionId)))
        self.driver.find_element_by_id(DescriptionId).clear()
        self.driver.find_element_by_id(DescriptionId).send_keys(new_description)

    def changeTypeToBug(self):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, IssueTypeFieldId)))
        self.driver.find_element_by_id(IssueTypeFieldId).clear()
        self.driver.find_element_by_id(IssueTypeFieldId).send_keys('Bug', Keys.ENTER)

        
    def updateButton(self):
        self.driver.find_element_by_id(UpdateButton).click()
        WebDriverWait(self.driver, 5).until_not(EC.visibility_of_element_located((By.XPATH, CreateIssueWindowXPATH)))










    






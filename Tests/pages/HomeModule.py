from pages.LogInModule import LoginPage
from pages.IssueModule import IssuePage
from pages.IssueFoundModule import IssueFound
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


BaseURl = "https://jira.atlassian.com/browse/TST"
HomePageTitle = "Agile Board - Atlassian JIRA"
LoginButtonXpath = ".//*[@id='user-options']/a"
CreateMenuID = "create_link"
IssuesDropId = 'find_link'
IssuesReportedByMeId = 'filter_lnk_reported_lnk'
FirstIssueXpath = "(.//*[@class='issue-link-key'])[1]"
SearchFieldID = 'quickSearchInput'


class HomePage:

    def __init__(self, driver):
        self.driver = driver
        
    def openHomepage(self):
        self.driver.get(BaseURl)
        assert HomePageTitle in self.driver.title

    def logInButton(self):
        self.driver.find_element_by_xpath(LoginButtonXpath).click()
        return LoginPage(self.driver)

    def createButton(self):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, CreateMenuID)))
        self.driver.find_element_by_id(CreateMenuID).click()
        return IssuePage(self.driver)

    def openIssues(self):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, IssuesDropId)))
        self.driver.find_element_by_id(IssuesDropId).click()
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, IssuesReportedByMeId)))
        self.driver.find_element_by_id(IssuesReportedByMeId).click()

    def verifyCreatedIssue(self):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, FirstIssueXpath)))        
        with open('pages/temp.txt','w') as t:
            t.write(self.driver.find_element_by_xpath(FirstIssueXpath).text)

    def searchLastIssue(self):
        WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.ID, SearchFieldID)))
        with open('pages/temp.txt','r') as t:
            issueNumber = t.readline()
        self.driver.find_element_by_id(SearchFieldID).send_keys(issueNumber, Keys.ENTER)
        return IssueFound(self.driver)






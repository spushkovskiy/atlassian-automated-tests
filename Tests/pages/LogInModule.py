from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


usernameID = "username"
passwordID = "password"
loginButtonID="login-submit"


class LoginPage():
    def __init__(self, driver):
        self.driver = driver

    def enterCredentials(self, email, password):
        wait = WebDriverWait(self.driver, 5)
        wait.until(EC.visibility_of_element_located((By.ID, usernameID)))
        wait.until(EC.visibility_of_element_located((By.ID, passwordID)))

        self.driver.find_element_by_id(usernameID).send_keys(email)
        self.driver.find_element_by_id(passwordID).send_keys(password)

        self.driver.find_element_by_id(loginButtonID).click()

        wait.until(EC.presence_of_element_located((By.XPATH, ".//*[@id='header-details-user-fullname']/span/span/img")))


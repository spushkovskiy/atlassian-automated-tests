import unittest
from pages.BaseModule import BaseClass
from pages.HomeModule import HomePage
from pages.LogInModule import LoginPage
from pages.IssueModule import IssuePage
from pages.IssueFoundModule import IssueFound
from pages.GlobalVars import UserCredentials


new_summary = "TEST UPDATED Atlassian Testing Issue"
new_description = "This is UPDATED description for Issue"

class EditIssue(BaseClass):
    def test2_edit_created_issue(self):
        home = HomePage(self.driver)
        home.openHomepage()
        login = home.logInButton()
        login.enterCredentials(UserCredentials['name'], UserCredentials['password'])
        search = home.searchLastIssue()
        issue = search.editIssueButton()
        issue.changeSummary(new_summary)
        issue.changeDescription(new_description)
        issue.changeTypeToBug()
        issue.updateButton()
        search.verifyUpdate(new_summary)

if __name__ == '__main__':
    unittest.main()
import unittest
import test_CreateNewIssue
import test_EditIssue
import test_SearchCreatedIssue


loader = unittest.TestLoader()
suite = loader.loadTestsFromModule(test_CreateNewIssue)
suite.addTest(loader.loadTestsFromModule(test_EditIssue))
suite.addTest(loader.loadTestsFromModule(test_SearchCreatedIssue))


runner = unittest.TextTestRunner(verbosity=2)
result = runner.run(suite)






import unittest
from selenium import webdriver
from pages.BaseModule import BaseClass
from pages.HomeModule import HomePage
from pages.GlobalVars import UserCredentials

summary = "TEST Atlassian Testing Issue Number 10"
description = "Short description for TEST Atlassian Testing Issue"


class CreateNewIssue(BaseClass):
    def test1_create_new_issue(self):
        home = HomePage(self.driver)
        home.openHomepage()
        login = home.logInButton()
        login.enterCredentials(UserCredentials['name'], UserCredentials['password'])
        issue = home.createButton()
        issue.createNewIssue(summary, description)
        home.openIssues()
        home.verifyCreatedIssue()


if __name__ == '__main__':
    unittest.main()
